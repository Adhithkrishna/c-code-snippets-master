﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Persistence;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace CCMERP.Service.Features.DefaultValuesService.Queries
{
    public class GetDefaultValuesByIdQuery : IRequest<Response<DefaultValues>>
    {
        public int defaultvaluesID { get; set; }

        public class GetDefaultValuesByIdHandler : IRequestHandler<GetDefaultValuesByIdQuery, Response<DefaultValues>>
        {
            private readonly IdentityContext _context;

            public GetDefaultValuesByIdHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<DefaultValues>> Handle(GetDefaultValuesByIdQuery request, CancellationToken cancellationToken)
            {

                DefaultValues defaultvalues = new DefaultValues();
                try
                {

                    defaultvalues = (from t1 in _context.defaultvalues
                                 //join cm in _context.country_master on t1.CountryID equals cm.CountryID

                                 where t1.DefaultValueID == request.defaultvaluesID
                                     select new DefaultValues
                                 {
                                     DefaultValueID = t1.DefaultValueID,
                                     DefaultName = t1.DefaultName,
                                     DefaultValue1 = t1.DefaultValue1,
                                     DefaultValue2 = DateTime.Now,
                                     DefaultValue3 = t1.DefaultValue3,
                                     DefaultValue4 = t1.DefaultValue4,
                                     DefaultValue5 = t1.DefaultValue5,
                                     DefaultValue6 = t1.DefaultValue6,
                                     DefaultPhone = t1.DefaultPhone,
                                     DefaultTiny = t1.DefaultTiny,
                                     DefaultmediumInt = t1.DefaultmediumInt,
                                     DefaultTinyText = t1.DefaultTinyText,
                                     DefaultLongText = t1.DefaultLongText,
                                     DefaultMedium = t1.DefaultMedium,
                                     DefaultDate = DateTime.Today,
                                     DefaultYear = DateTime.Now.ToString("yyyy"),
                                     DefaultTime = DateTime.Now.ToString("hh:mm:ss"),
                                     DefaultChar = t1.DefaultChar,
                                     DefaultBit = t1.DefaultBit
                                         // DefaultTime = DateTime.Now.ToLongTimeString()
                                         // DefaultDate = DateTime.Now.ToString("dd-MM-yyyy"),

                                         // DefaultDate = t1.DefaultDate.ToString("dd-MM-yyyy")


                                     }).FirstOrDefault();

                    if (defaultvalues != null)
                    {
                        return await Task.FromResult(new Response<DefaultValues>(defaultvalues, "Success", true));
                    }
                    else
                    {


                        return await Task.FromResult(new Response<DefaultValues>(defaultvalues, "No record found ", false));
                    }
                }
                catch (Exception ex)
                {
                    var objError = ex;
                    return await Task.FromResult(new Response<DefaultValues>(defaultvalues, "Exception", false));
                }

            }
        }



    }
}
