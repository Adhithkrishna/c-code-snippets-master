﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Defaultvalues.Response;
using CCMERP.Domain.Entities;
using CCMERP.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.DefaultValuesService.Queries
{
    public class GetAllDefaultValuesQuery : IRequest<Response<GetAllDefaultValuesResponse>>
    {

        public GetAllDefaultValuesQuery()
        {

        }
        const int maxPageSize = 50;
        public int PageNumber { get; set; }
        public int _pageSize { get; set; }
        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }

        public class GetAllDefaultValuesQueryHandler : IRequestHandler<GetAllDefaultValuesQuery, Response<GetAllDefaultValuesResponse>>
        {
            private readonly IdentityContext _context;
            public GetAllDefaultValuesQueryHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<GetAllDefaultValuesResponse>> Handle(GetAllDefaultValuesQuery request, CancellationToken cancellationToken)
            {

                GetAllDefaultValuesResponse getAllDefaultValuesResponse = new GetAllDefaultValuesResponse();
                try
                {
                    var DefaultList = await
                        (
                       from d in _context.defaultvalues
                       select new DefaultValues()
                       {
                           DefaultValueID = d.DefaultValueID,
                           DefaultName = d.DefaultName,
                           DefaultValue1 = d.DefaultValue1,
                           DefaultValue2 = DateTime.Now,
                           DefaultValue3 = d.DefaultValue3,
                           DefaultValue4 = d.DefaultValue4,
                           DefaultValue5 = d.DefaultValue5,
                           DefaultValue6 = d.DefaultValue6,
                           DefaultPhone = d.DefaultPhone,
                           DefaultTiny = d.DefaultTiny,
                           DefaultmediumInt = d.DefaultmediumInt,
                           DefaultTinyText = d.DefaultTinyText,
                           DefaultLongText = d.DefaultLongText,
                           DefaultMedium = d.DefaultMedium,
                           DefaultDate = DateTime.Today,
                           DefaultYear = DateTime.Now.ToString("yyyy"),                        
                           DefaultTime = DateTime.Now.ToString("hh:mm:ss"),
                           DefaultChar  =  d.DefaultChar,
                           DefaultBit = d.DefaultBit
                           //DefaultTime = DateTime.Now.ToLongTimeString()
                           //DefaultDate = DateTime.Now.ToString("dd-MM-yyyy"),

                           //CountryID = p.CountryID,
                           // CountryName = cm.CountryName

                       }).Distinct().ToListAsync();
                    if (DefaultList.Count == 0)
                    {

                        return new Response<GetAllDefaultValuesResponse>(getAllDefaultValuesResponse, "No record found ", false);
                    }
                    else
                    {
                        getAllDefaultValuesResponse.TotalItems = DefaultList.Count;
                        getAllDefaultValuesResponse.TotalPages = (int)Math.Ceiling(getAllDefaultValuesResponse.TotalItems / (double)request.PageSize);
                        getAllDefaultValuesResponse.defaultvalues = DefaultList.Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize).ToList();
                        return new Response<GetAllDefaultValuesResponse>(getAllDefaultValuesResponse, "Success", true);
                    }
                }
                catch (Exception ex)
                {
                    var error = ex;

                    return new Response<GetAllDefaultValuesResponse>(getAllDefaultValuesResponse, "Exception", false);
                }


            }
        }



    }
}
