﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Persistence;
using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.DefaultValuesService.Commands
{
    public class CreateDefaultValuesCommand : IRequest<Response<int>>
    {


        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string DefaultName { get; set; }
        [Required]
        public decimal DefaultValue1 { get; set; }
        public DateTime DefaultValue2 { get; set; }
        public double DefaultValue3 { get; set; }
        public float DefaultValue4 { get; set; }
        public short DefaultValue5 { get; set; }
        public string DefaultValue6 { get; set; }
       public long DefaultPhone { get; set; }
        public byte DefaultTiny { get; set; }
        public int DefaultmediumInt { get; set; }

        public string DefaultTinyText { get; set; }
        public string DefaultLongText { get; set; }
        public string DefaultMedium { get; set; }
        public DateTime DefaultDate { get; set; }
        public DateTime DefaultTime { get; set; }
        public DateTime DefaultYear { get; set; }
        public string DefaultChar { get; set; }
        public bool DefaultBit { get; set; }


        public class CreateDefaultValuesCommandHandler : IRequestHandler<CreateDefaultValuesCommand, Response<int>>
        {
            private readonly IdentityContext _context;
            //private readonly ITransactionDbContext _tcontext;
            public CreateDefaultValuesCommandHandler(IdentityContext context)//, ITransactionDbContext tcontext)
            {
                _context = context;
                //_tcontext = tcontext;
            }
            public async Task<Response<int>> Handle(CreateDefaultValuesCommand request, CancellationToken cancellationToken)
            {

                try
                {

                    var def = _context.defaultvalues.Where(a => a.DefaultName.ToLower() == request.DefaultName.ToLower()).ToList();
                    if (def.Count > 0)
                    {
                        return new Response<int>(0, "An Value with the same name already exists", false);
                    }
                    else
                    {

                        DefaultValues defaultvalues = new DefaultValues()
                        {
                            DefaultName = request.DefaultName,
                            DefaultValue1 = request.DefaultValue1,
                            DefaultValue2 = DateTime.Now,
                            DefaultValue3 = request.DefaultValue3,
                            DefaultValue4 = request.DefaultValue4,
                            DefaultValue5 = request.DefaultValue5,
                            DefaultValue6 = request.DefaultValue6,
                            DefaultPhone = request.DefaultPhone,
                            DefaultTiny  = request.DefaultTiny,
                            DefaultmediumInt = request.DefaultmediumInt,
                            DefaultTinyText = request.DefaultTinyText,
                            DefaultLongText = request.DefaultLongText,
                            DefaultMedium = request.DefaultMedium,
                            DefaultDate = DateTime.Today,
                            DefaultYear = DateTime.Now.ToString("yyyy"),
                            DefaultTime = DateTime.Now.ToString("hh:mm:ss"),
                            DefaultChar = request.DefaultChar,
                            DefaultBit =  request.DefaultBit
                            //DefaultDate = DateTime.Now.ToString("dd-MM-yyyy"),
                            //DefaultTime = DateTime.Now.ToLongTimeString()
                            // CountryID = request.CountryID

                        };


                        _context.defaultvalues.Add(defaultvalues);
                        await _context.SaveChangesAsync();

                        return new Response<int>(defaultvalues.DefaultValueID, "Success", true);
                    }


                }
                catch (Exception ex)
                {

                    var currentExpection = ex;
                    return new Response<int>(0, "Exception", false);
                }


            }
        }



    }
}
