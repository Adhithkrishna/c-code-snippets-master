﻿using CCMERP.Domain.Common;
using CCMERP.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.DefaultValuesService.Commands
{
    public class DeleteDefaultValuesByIdCommand : IRequest<Response<int>>
    {
        [Required]
        //[Range(0, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        //[RegularExpression("([1-9]+)", ErrorMessage = "Please enter valid Number")]
        public int DefaultValueID { get; set; }
        public class DeleteDefaultValuesByIdCommandHandler : IRequestHandler<DeleteDefaultValuesByIdCommand, Response<int>>
        {
            private readonly IdentityContext _context;
            public DeleteDefaultValuesByIdCommandHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<int>> Handle(DeleteDefaultValuesByIdCommand request, CancellationToken cancellationToken)
            {
                try
                {
                    var values = await _context.defaultvalues.FindAsync(request.DefaultValueID);
                    if (values == null)
                    {
                        return new Response<int>(0, "No values found ", false);
                    }
                    else
                    {
                        var deleteDefaultValuesId = request.DefaultValueID;
                        // currency.IsActive = 0;
                        _context.Remove(values);
                        await _context.SaveChangesAsync();

                        //var org = _context.OrganizationUserMapping.FindAsync(request.Org_ID);

                        return new Response<int>(values.DefaultValueID, "Value successfully deactivated", true);
                    }
                }
                catch (Exception)
                {
                    return new Response<int>(0, "Exception", false);
                }


            }
        }
    }
}
