﻿using CCMERP.Domain.Common;
using CCMERP.Persistence;
using MediatR;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.DefaultValuesService.Commands
{
    public class UpdateDefaultValuesCommand : IRequest<Response<int>>
    {
        [Required]
        public int DefaultValueID { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string DefaultName { get; set; }
        [Required]
        
        public decimal DefaultValue1 { get; set; }
        public DateTime DefaultValue2 { get; set; }
        public double DefaultValue3 { get; set; }
        public float DefaultValue4 { get; set; }
        public short DefaultValue5 { get; set; }
        public string DefaultValue6 { get; set; }
        public long DefaultPhone { get; set; }
        public byte DefaultTiny { get; set; }
        public int DefaultmediumInt { get; set; }
        public string DefaultTinyText { get; set; }
        public string DefaultLongText { get; set; }
        public string DefaultMedium { get; set; }
        public DateTime DefaultDate { get; set; }

        public DateTime DefaultTime { get; set; }
        public DateTime DefaultYear { get; set; }
        public string DefaultChar { get; set; }
        public bool DefaultBit { get; set; }


        public class UpdateDefaultValuesCommandHandler : IRequestHandler<UpdateDefaultValuesCommand, Response<int>>
        {
            private readonly IdentityContext _context;
            public UpdateDefaultValuesCommandHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<int>> Handle(UpdateDefaultValuesCommand request, CancellationToken cancellationToken)
            {
                try
                {


                    var def = _context.defaultvalues.Where(a => a.DefaultValueID == request.DefaultValueID).FirstOrDefault();

                    if (def == null)
                    {
                        return new Response<int>(0, "No Values found", true);
                    }
                    else
                    {

                        def.DefaultName = request.DefaultName;
                        def.DefaultValue1 = request.DefaultValue1;
                        def.DefaultValue2 = DateTime.Now;
                        def.DefaultValue3 = request.DefaultValue3;
                        def.DefaultValue4 = request.DefaultValue4;
                        def.DefaultValue5 = request.DefaultValue5;
                        def.DefaultValue6 = request.DefaultValue6;
                        def.DefaultPhone = request.DefaultPhone;
                        def.DefaultTiny = request.DefaultTiny;
                        def.DefaultmediumInt = request.DefaultmediumInt;
                        def.DefaultTinyText = request.DefaultTinyText;
                        def.DefaultLongText = request.DefaultLongText;
                        def.DefaultMedium = request.DefaultMedium;
                        def.DefaultDate = DateTime.Today;
                        def.DefaultYear = DateTime.Now.ToString("yyyy");
                        def.DefaultTime = DateTime.Now.ToString("hh:mm:ss");
                        def.DefaultChar = request.DefaultChar;
                        def.DefaultBit = request.DefaultBit;
                        // def.DefaultTime = DateTime.Now.ToLongTimeString();
                        // def.DefaultDate = DateTime.Now.ToString("dd-MM-yyyy");



                        _context.defaultvalues.Update(def);
                        await _context.SaveChangesAsync();

                        return new Response<int>(def.DefaultValueID, "Success", true);
                    }
                }
                catch (Exception ex)
                {
                    var currentExpection = ex;
                    return new Response<int>(0, "Exception", false);
                }
            }
        }


    }
}
