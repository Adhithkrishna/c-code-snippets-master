﻿using CCMERP.Domain.Common;
using CCMERP.Persistence;
using MediatR;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.CurrencyService.Commands
{
    public class UpdateCurrencyCommand : IRequest<Response<int>>
    {
        [Required]
        public int CurrencyID { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string CurrencyName { get; set; }
        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string CurrencyABR { get; set; }
        [Required]
        public int CountryID { get; set; }

        public class UpdateCurrencyCommandHandler : IRequestHandler<UpdateCurrencyCommand, Response<int>>
        {
            private readonly IdentityContext _context;
            public UpdateCurrencyCommandHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<int>> Handle(UpdateCurrencyCommand request, CancellationToken cancellationToken)
            {
                try
                {


                    var cur = _context.currency_master.Where(a => a.CurrencyID == request.CurrencyID).FirstOrDefault();

                    if (cur == null)
                    {
                        return new Response<int>(0, "No currency found", true);
                    }
                    else
                    {

                        cur.CurrencyName = request.CurrencyName;
                        cur.CurrencyABR = request.CurrencyABR;
                        cur.CountryID = request.CountryID;


                        _context.currency_master.Update(cur);
                        await _context.SaveChangesAsync();

                        return new Response<int>(cur.CurrencyID, "Success", true);
                    }
                }
                catch (Exception)
                {
                    return new Response<int>(0, "Exception", false);
                }
            }
        }


    }
}
