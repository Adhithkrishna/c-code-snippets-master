﻿using CCMERP.Domain.Common;
using CCMERP.Persistence;
using MediatR;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.CurrencyService.Commands
{
    public class UpdatePrivilegeCommand : IRequest<Response<int>>
    {
        [Required]
        public int PrivilegeID { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string PrivilegeName { get; set; }
        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string PrivilegeCode { get; set; }
        [Required]
        public double PrivilegeAmount { get; set; }
        [Required]
        public float PrivilegePrice { get; set; }
        [Required]
        public DateTime PrivilegeDate { get; set; }

        public class UpdatePrivilegeCommandHandler : IRequestHandler<UpdatePrivilegeCommand, Response<int>>
        {
            private readonly IdentityContext _context;
            public UpdatePrivilegeCommandHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<int>> Handle(UpdatePrivilegeCommand request, CancellationToken cancellationToken)
            {
                try
                {


                    var pri = _context.privilege.Where(a => a.PrivilegeID == request.PrivilegeID).FirstOrDefault();

                    if (pri == null)
                    {
                        return new Response<int>(0, "No Privilege found", true);
                    }
                    else
                    {

                        pri.PrivilegeName = request.PrivilegeName;
                        pri.PrivilegeCode = request.PrivilegeCode;
                        pri.PrivilegeAmount = request.PrivilegeAmount;
                        pri.PrivilegePrice = request.PrivilegePrice;
                        pri.PrivilegeDate = request.PrivilegeDate;
                       


                        _context.privilege.Update(pri);
                        await _context.SaveChangesAsync();

                        return new Response<int>(pri.PrivilegeID, "Success", true);
                    }
                }
                catch (Exception)
                {
                    return new Response<int>(0, "Exception", false);
                }
            }
        }


    }
}
