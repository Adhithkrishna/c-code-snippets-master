﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Persistence;
using MediatR;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.CurrencyService.Commands
{
    public class CreatePrivilegeCommand : IRequest<Response<int>>
    {


        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string PrivilegeName { get; set; }
        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string PrivilegeCode { get; set; }
        [Required]
        public double PrivilegeAmount { get; set; }
        [Required]
        public float PrivilegePrice { get; set; }
        [Required]
        public DateTime PrivilegeDate { get; set; }


        public class CreatePrivilegeCommandHandler : IRequestHandler<CreatePrivilegeCommand, Response<int>>
        {
            private readonly IdentityContext _context;
            //private readonly ITransactionDbContext _tcontext;
            public CreatePrivilegeCommandHandler(IdentityContext context)//, ITransactionDbContext tcontext)
            {
                _context = context;
                //_tcontext = tcontext;
            }
            public async Task<Response<int>> Handle(CreatePrivilegeCommand request, CancellationToken cancellationToken)
            {

                try
                {

                    var pri = _context.privilege.Where(a => a.PrivilegeName.ToLower() == request.PrivilegeName.ToLower()).ToList();
                    if (pri.Count > 0)
                    {
                        return new Response<int>(0, "An privilege with the same name already exists", false);
                    }
                    else
                    {

                        Privilege privilege = new Privilege()
                        {
                            PrivilegeName = request.PrivilegeName,
                            PrivilegeCode = request.PrivilegeCode,
                            PrivilegeAmount = request.PrivilegeAmount,
                            PrivilegePrice = request.PrivilegePrice,
                           PrivilegeDate = request.PrivilegeDate

                        };


                        _context.privilege.Add(privilege);
                        await _context.SaveChangesAsync();

                        return new Response<int>(privilege.PrivilegeID, "Success", true);
                    }


                }
                catch (Exception ex)
                {

                    var currentExpection = ex;
                    return new Response<int>(0, "Exception", false);
                }


            }
        }



    }
}
