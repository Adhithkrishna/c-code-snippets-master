﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Domain.Currency.Response;
using CCMERP.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace CCMERP.Service.Features.CurrencyService.Queries
{
    public class GetPrivilegeByIdQuery : IRequest<Response<Privilege>>
    {
        public int privilegeId { get; set; }

        public class GetPrivilegeByIdQueryHandler : IRequestHandler<GetPrivilegeByIdQuery, Response<Privilege>>
        {
            private readonly IdentityContext _context;

            public GetPrivilegeByIdQueryHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<Privilege>> Handle(GetPrivilegeByIdQuery request, CancellationToken cancellationToken)
            {

                Privilege privilege = new Privilege();
                try
                {

                    privilege = (from t1 in _context.privilege
                                 join cm in _context.country_master on t1.CountryID equals cm.CountryID

                                 where t1.PrivilegeID == request.privilegeId
                                select new Privilege
                                {
                                    PrivilegeID = t1.PrivilegeID,
                                    PrivilegeName = t1.PrivilegeName,
                                    PrivilegeCode = t1.PrivilegeCode,
                                    PrivilegeAmount = t1.PrivilegeAmount,
                                    PrivilegePrice = t1.PrivilegePrice,
                                    PrivilegeDate = DateTime.Now,
                                    CountryID = t1.CountryID,
                                    CountryName = cm.CountryName


                                }).FirstOrDefault();

                    if (privilege != null)
                    {
                        return await Task.FromResult(new Response<Privilege>(privilege, "Success", true));
                    }
                    else
                    {


                        return await Task.FromResult(new Response<Privilege>(privilege, "No record found ", false));
                    }
                }
                catch (Exception)
                {

                    return await Task.FromResult(new Response<Privilege>(privilege, "Exception", false));
                }

            }
        }



    }
}
