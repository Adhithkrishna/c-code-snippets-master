﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Domain.Privileges.Response;
using CCMERP.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.PrivilegeService.Queries
{
    public class GetAllPrivilegeQuery : IRequest<Response<GetAllPrivilegeResponse>>
    {
        public GetAllPrivilegeQuery()
        {

        }
        const int maxPageSize = 50;
        public int PageNumber { get; set; }
        public int _pageSize { get; set; }
        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }

        public class GetAllPrivilegeQueryHandler : IRequestHandler<GetAllPrivilegeQuery, Response<GetAllPrivilegeResponse>>
        {
            private readonly IdentityContext _context;
            public GetAllPrivilegeQueryHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<GetAllPrivilegeResponse>> Handle(GetAllPrivilegeQuery request, CancellationToken cancellationToken)
            {

                GetAllPrivilegeResponse getAllPrivilegeResponse = new GetAllPrivilegeResponse();
                try
                {
                    var PrivilegeList = await
                        (
                       from p in _context.privilege
                       join cm in _context.country_master on p.CountryID equals cm.CountryID
                       select new Privilege()
                       {
                            PrivilegeID = p.PrivilegeID,
                            PrivilegeName = p.PrivilegeName,
                            PrivilegeCode = p.PrivilegeCode,
                            PrivilegeAmount = p.PrivilegeAmount,
                            PrivilegePrice = p.PrivilegePrice,
                            PrivilegeDate = DateTime.Now,
                             CountryID = p.CountryID,
                           CountryName = cm.CountryName

                        }).Distinct().ToListAsync();
                    if (PrivilegeList.Count == 0)
                    {

                        return new Response<GetAllPrivilegeResponse>(getAllPrivilegeResponse, "No record found ", false);
                    }
                    else
                    {
                        getAllPrivilegeResponse.TotalItems = PrivilegeList.Count;
                        getAllPrivilegeResponse.TotalPages = (int)Math.Ceiling(getAllPrivilegeResponse.TotalItems / (double)request.PageSize);
                        getAllPrivilegeResponse.Privileges = PrivilegeList.Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize).ToList();
                        return new Response<GetAllPrivilegeResponse>(getAllPrivilegeResponse, "Success", true);
                    }
                }
                catch (Exception ex)
                {
                    var error = ex;

                    return new Response<GetAllPrivilegeResponse>(getAllPrivilegeResponse, "Exception", false);
                }


            }
        }



    }
}
