﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Domain.Order.Data;
using CCMERP.Persistence;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.Order.Commands
{
    public class UploadOrderCommand : IRequest<Response<int>>
	{
		public IFormFile file { get; set; }
		public int orgId { get; set; }
		public int customerId { get; set; }
		public class UploadOrderCommandHandler : IRequestHandler<UploadOrderCommand, Response<int>>
		{
			private readonly ITransactionDbContext _context;
			private readonly IdentityContext _icontext;
			public UploadOrderCommandHandler(ITransactionDbContext context, IdentityContext icontext)
			{
				_context = context;
				_icontext = icontext;
			}
			public async Task<Response<int>> Handle(UploadOrderCommand request, CancellationToken cancellationToken)
			{
				List<ImportOrder> addItemsRequests = new List<ImportOrder>();
				var transaction = _context.Database.BeginTransaction();
				try
				{
					//var extension = "." + request.file.FileName.Split('.')[request.file.FileName.Split('.').Length - 1];

					if (request.file.FileName.EndsWith(".csv"))
					{

						using (var sreader = new StreamReader(request.file.OpenReadStream()))
						{
							string[] headers = sreader.ReadLine().Split(',');     //Title
							string[] headers1 = { "LINE_ID", "DUE_DATE", "STOCK_CODE", "QTY_REQUIRED" };     //Title

							if (headers.SequenceEqual(headers1))
							{

								while (!sreader.EndOfStream)                          //get all the content in rows 
								{
									string[] rows = sreader.ReadLine().Split(',');
									ImportOrder item = new ImportOrder();

									if (rows[0].Trim().Length > 0)
									{
										string[] lineId = rows[0].ToString().Trim().Split(".");
										if (lineId.Length > 0)
										{
											item.LINE_ID = int.Parse(lineId[0]);
										}
										else
										{
											item.LINE_ID = 0;

										}
									}
									else
									{
										item.LINE_ID = 0;

									}

									if (rows[1].Trim().Length > 0)
									{

										if (rows[1].Trim().Length > 0)
										{
											string[] sod = rows[1].Trim().Split("-");
											item.DUE_DATE = DateTime.ParseExact((sod[2] + "-" + sod[1] + "-" + sod[0]), "yyyy-MM-dd", CultureInfo.InvariantCulture);
										}
										else
										{
											item.DUE_DATE = DateTime.Now;

										}
                                    }
                                    else
                                    {
										item.DUE_DATE = DateTime.Now;
									}


										item.STOCK_CODE = rows[2].ToString().Trim();

									if (rows[3].Trim().Length > 0)
									{
										string[] qtyReq = rows[3].ToString().Trim().Split(".");
										if (qtyReq.Length > 0)
										{
											item.QTY_REQUIRED = int.Parse(qtyReq[0]);
										}
										else
										{
											item.QTY_REQUIRED = 0;

										}
									}
									else
									{
										item.QTY_REQUIRED = 0;

									}

									addItemsRequests.Add(item);
								}


								var anyDuplicate = addItemsRequests.GroupBy(x => x.STOCK_CODE).Any(g => g.Count() > 1);
								SalesOrderHeader salesOrderHeader = new SalesOrderHeader();
								if (anyDuplicate)
								{
									return new Response<int>(0, "Csv file contains duplicate order item entries", false);
								}
								else
								{
									if (addItemsRequests.Count > 0)
									{
										using (transaction)
										{
											transaction.CreateSavepoint("salesOrder");
											var country = _icontext.country_master.ToList();
											var curency = _icontext.currency_master.ToList();
											var cust = _context.Customers.ToList();
											var org = _context.organizationCustomerMappings.ToList();
											string SNO = await KEYVALUE(request.orgId, 1);

											salesOrderHeader = (from t1 in cust
																join t2 in country on t1.BillingCountry equals t2.CountryID
																join t3 in country on t1.ShippingCountry equals t3.CountryID
																join t5 in org on t1.CustomerID equals t5.CustomerID
																join t4 in curency on t1.CurrencyId equals t4.CurrencyID
																where t1.CustomerID == request.customerId
																select new SalesOrderHeader
																{
																	ShippingAddress1 = t1.ShippingAddress1,
																	ShippingAddress2 = t1.ShippingAddress2,
																	ShippingCity = t1.ShippingCity,
																	ShippingState = t1.ShippingState,
																	ShippingCountry = t1.ShippingCountry,
																	shippingCountryName = t3.CountryName,
																	ShippingZipCode = t1.ShippingZipCode,
																	BillingAddress1 = t1.BillingAddress1,
																	BillingAddress2 = t1.BillingAddress2,
																	BillingCity = t1.BillingCity,
																	BillingState = t1.BillingState,
																	BillingCountry = t1.BillingCountry,
																	billingCountryName = t2.CountryName,
																	BillingZipCode = t1.BillingZipCode,
																	CurrencyId = t1.CurrencyId,
																	currency = t4.CurrencyName,
																	CustomerId = request.customerId,
																	ExpectedDate = DateTime.Now,
																	OrgId = request.orgId,
																	SODate = DateTime.Now,
																	SONo = SNO,
																	StatusId = 1
																}).FirstOrDefault();

											_context.salesorderheader.Add(salesOrderHeader);
											await _context.SaveChangesAsync();


											foreach (var item in addItemsRequests)
											{
												var item1 = _context.itemmaster.Where(a => a.OrgId == request.orgId && a.ItemCode == item.STOCK_CODE).FirstOrDefault();
												if (item1 != null)
												{
													SalesOrderDtl orderDtl = new SalesOrderDtl();
													orderDtl.ItemId = item1.ItemId;
													orderDtl.OrgId = request.orgId;
													orderDtl.ExpectedDate = item.DUE_DATE;
													orderDtl.Quantity = item.QTY_REQUIRED;
													orderDtl.SOHdrId = salesOrderHeader.SOHdrId;
													_context.salesorderdtl.Add(orderDtl);
													await _context.SaveChangesAsync();
												}
												else
												{
													transaction.RollbackToSavepoint("salesOrder");
													return new Response<int>(0, "Uploaded  stock code not in(" + item.STOCK_CODE + ") item master", false);

												}
											}

											transaction.Commit();
										}
										

										
									}
									
									
								}
								return new Response<int>(salesOrderHeader.SOHdrId, "Success", true);
							}
							else
							{
								return new Response<int>(0, "Please upload a valid csv file", true);
							}

						}

					}
					else
					{
						return new Response<int>(0, "Please upload a valid csv file", false);
					}
				}
				catch (Exception ex)
				{
					transaction.RollbackToSavepoint("salesOrder");
					return new Response<int>(0, "Exception", false);
				}
			}

			public async Task<string> KEYVALUE(int orgId, int keys)
			{
				string SNO = string.Empty;
				try
				{
					KeyMaster keyMaster = _context.keymaster.Find(keys, orgId);
					long keyval = (keyMaster.Value + 1);
					keyMaster.Value = keyval;
					_context.keymaster.Update(keyMaster);
					await _context.SaveChangesAsync();
					SNO = $"{orgId.ToString().PadRight(3, '0')}{keyval.ToString().PadLeft(8, '0')}";

				}
				catch (Exception ex)
				{


				}
				return SNO;

			}
		}

	}

}