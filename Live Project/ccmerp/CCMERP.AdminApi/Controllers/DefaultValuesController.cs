﻿using CCMERP.Service.Features.DefaultValuesService.Commands;
using CCMERP.Service.Features.DefaultValuesService.Queries;
using CCMERP.Service.Features.OrganizationsService.Commands;
using CCMERP.Service.Features.OrganizationsService.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;


namespace CCMERP.AdminApi.Controllers
{
    [Route("api/V2/DefaultValues")]
    [ApiController]
    public class DefaultValuesController : ControllerBase
    {
        private IMediator _mediator;
        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();

        [HttpGet]
        //[Authorize]
        public async Task<IActionResult> GetAll(int pageNumber = 1, int pageSize = 25)
        {
            return Ok(await Mediator.Send(new GetAllDefaultValuesQuery { PageNumber = pageNumber, _pageSize = pageSize }));
        }

        [HttpGet("{defaultvaluesID}")]
        public async Task<IActionResult> GetById(int defaultvaluesID)
        {
            return Ok(await Mediator.Send(new GetDefaultValuesByIdQuery { defaultvaluesID = defaultvaluesID }));
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateDefaultValuesCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpDelete("{defaultvaluesID}")]
        public async Task<IActionResult> Delete(int defaultvaluesID)
        {
            return Ok(await Mediator.Send(new DeleteDefaultValuesByIdCommand { DefaultValueID = defaultvaluesID }));
        }

        [HttpPut]
        public async Task<IActionResult> Update(UpdateDefaultValuesCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

    }
}
