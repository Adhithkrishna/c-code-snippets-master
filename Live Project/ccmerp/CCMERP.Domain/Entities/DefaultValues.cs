﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCMERP.Domain.Entities
{
   public class DefaultValues
    {
        [Key]
        public int DefaultValueID { get; set; }
        public string DefaultName { get; set; }
        public decimal DefaultValue1 { get; set; }
        public DateTime DefaultValue2 { get; set; }
        public double DefaultValue3 { get; set; }
        public float DefaultValue4 { get; set; }
        public short DefaultValue5 { get; set; }
        public string DefaultValue6 { get; set; }
        public long DefaultPhone { get; set; }
        public byte DefaultTiny { get; set; }
        public int DefaultmediumInt { get; set; }
        public string DefaultTinyText { get; set; }
        public string DefaultLongText { get; set; }
        public string DefaultMedium { get; set; }
        
       public string DefaultTime { get; set; }
       public DateTime DefaultDate { get; set; }
       public string DefaultYear { get; set; } 
       public string DefaultChar { get; set; }
        public bool DefaultBit { get; set; }
    }
}
