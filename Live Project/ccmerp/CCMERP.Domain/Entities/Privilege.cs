﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCMERP.Domain.Entities
{
     public class Privilege
    {
        [Key]
        public int PrivilegeID { get; set; }
        public string PrivilegeName { get; set; }
        public string PrivilegeCode { get; set; }
        public double PrivilegeAmount { get; set; }
        public float PrivilegePrice { get; set; }
        public DateTime PrivilegeDate { get; set; }
        public int CountryID { get; set; }
        [NotMapped]
        public string CountryName { get; set; }

    }
}
