﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCMERP.Domain.Auth
{
    public class CustomerOrganizations
    {
        public int Org_ID { get; set; }
        public int customerId { get; set; }
        public string Name { get; set; }
    }
}
