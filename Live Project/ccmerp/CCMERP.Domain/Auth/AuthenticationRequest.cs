﻿
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;

namespace CCMERP.Domain.Auth
{
    public class AuthenticationRequest
    {
        [JsonProperty(PropertyName = "Email")]
        public string Email { get; set; }
        [JsonProperty(PropertyName = "Password")]
        public string Password { get; set; }

    }

    public class TwoFactorAuthenticationRequest
    {
        public TwoFactorAuthenticationRequest()
        {
            orgId = 0;
        }

        [JsonProperty(PropertyName = "otp")]
        public string otp { get; set; }
        [JsonProperty(PropertyName = "Email")]
        public string Email { get; set; }

        public int orgId { get; set; }
    }
}
