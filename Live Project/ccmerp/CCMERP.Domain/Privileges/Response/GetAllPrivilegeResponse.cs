﻿using CCMERP.Domain.Entities;
using CCMERP.Domain.Pagination.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCMERP.Domain.Privileges.Response
{
    public class GetAllPrivilegeResponse : PaginationResponse
    {
        public List<Privilege> Privileges { get; set; }
    }
}
