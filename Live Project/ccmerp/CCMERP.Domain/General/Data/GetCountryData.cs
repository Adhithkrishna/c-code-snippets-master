﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCMERP.Domain.General.Data
{
    public class GetCountryData
    {
        public int countryId { get; set; }
        public string countryName { get; set; }
        public string countryAbr { get; set; }
        public int countryCode { get; set; }

    }
}
