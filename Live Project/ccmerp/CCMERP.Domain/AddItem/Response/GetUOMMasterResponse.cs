﻿using CCMERP.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCMERP.Domain.AddItem.Response
{
    public class GetUOMMasterResponse
    {
        public List<UOMMaster> uOMs { get; set; }
    }
}
