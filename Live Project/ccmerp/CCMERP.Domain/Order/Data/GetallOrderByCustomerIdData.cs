﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCMERP.Domain.Order.Data
{
   public class GetallOrderByCustomerIdData
    {
		public int sohdrId { get; set; }
		public int orgId { get; set; }
		public string orgName { get; set; }
		public int customerId { get; set; }
		public string customerName { get; set; }
		public string soNo { get; set; }
		public string soDate { get; set; }
		public string expectedDate { get; set; }
		public string status { get; set; }
		public int statusId { get; set; }
	}

	public class SalesOrderHeaderModel
	{
		
		public int sohdrId { get; set; }
		public int orgId { get; set; }
		public int customerId { get; set; }
		public string soNo { get; set; }
		public string soDate { get; set; }
		public string expectedDate { get; set; }
		public string remarks { get; set; }
		public string shippingAddress1 { get; set; }
		public string shippingAddress2 { get; set; }
		public string shippingCity { get; set; }
		public string shippingState { get; set; }
		public int shippingCountry { get; set; }
	
		public string shippingCountryName { get; set; }
		public string shippingZipCode { get; set; }
		public string billingAddress1 { get; set; }
		public string billingAddress2 { get; set; }
		public string billingCity { get; set; }
		public string billingState { get; set; }
		public int billingCountry { get; set; }
		
		public string billingCountryName { get; set; }
		public string billingZipCode { get; set; }
		
		public string currency { get; set; }
		public int currencyId { get; set; }
		public int statusId { get; set; }
		
		public string status { get; set; }
	}
}
