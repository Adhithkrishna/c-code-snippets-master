﻿using CCMERP.Domain.Entities;
using CCMERP.Domain.Pagination.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCMERP.Domain.Country.Response
{
    public class GetAllCountryResponse : PaginationResponse
    {

        public List<CountryMaster> Countries { get; set; }
    }
}
